#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>
#include <openvino_object_detection/Object.h>
#include <openvino_object_detection/Objects.h>
#include <ros/ros.h>
#include <sensor_msgs/Image.h>
using namespace cv;

int main(int argc, char **argv) {
  ros::init(argc, argv, "object_detection_test");
  // Handle creation
  ros::NodeHandle n;
  Mat frame;
  VideoCapture cap(argv[1]);
  ros::ServiceClient client =
      n.serviceClient<openvino_object_detection::Objects>("/tiny_yolov3");
  openvino_object_detection::Objects t;
  sensor_msgs::Image output_image_msg;
  cv::namedWindow("view");

  while (1) {
    cap >> frame;
    t.request.t = 0.5;
    t.request.iou_t = 0.8;
    t.request.img =
        *cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
    if (client.call(t)) {
      std::cout << t.response.ms << "\n";

      for (openvino_object_detection::Object obj : t.response.objects) {
        std::ostringstream conf;
        conf << ":" << std::fixed << std::setprecision(3) << obj.confidence;
        cv::putText(frame, (std::string)obj.label + conf.str(),
                    cv::Point2f(obj.x - obj.w / 2, obj.y - obj.h / 2 - 5),
                    cv::FONT_HERSHEY_COMPLEX_SMALL, 1, cv::Scalar(0, 0, 255), 1,
                    cv::LINE_AA);
        cv::rectangle(frame, cv::Point2f(obj.x - obj.w / 2, obj.y - obj.h / 2),
                      cv::Point2f(obj.x + obj.w / 2, obj.y + obj.h / 2),
                      cv::Scalar(0, 0, 255), 1, cv::LINE_AA);
      }

      cv::imshow("view", frame);
      
      cv::waitKey(1);
    }
    ros::spinOnce();
  }
}
