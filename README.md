# ros_openvino_tiny-yolov3

This repository contains ROS Service for Intel Openvino based yolov3 object detection. This was created for accelerating tiny-Yolov3 neural network algorithm by running it on intel NUC iGPU for underwater object recognition to be used in autonomous underwater vehicle (https://auvnitrkl.github.io/)
This works at around 15 fps on intel i7 6th Gen NUC, which is suitable for use in real time without creating any major power draw or consuming extra cores. So it keeps the main processor free for running other SLAM, path planning algorithms.
Intel OpenVino 2018 R5 suported

##Converting darknet model to openvino

Use **https://github.com/PINTO0309/OpenVINO-YoloV3/releases/tag/2018R5** release.
Only cpp demo tested

MOST IMPORTANT:
Use only with tensorflow-1.12.0. Other versions of tensorflow won't work.
